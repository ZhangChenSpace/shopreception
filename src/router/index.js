//配置路由的地方
import { createRouter,createWebHistory } from "vue-router";

const router=createRouter({
    //配置路由
    history:createWebHistory(),
    routes:[
        {
            name:"Home",
            path:"/Home",
            meta:{a:true},
            component:()=>import('@/pages/Home/Home.vue')
        },
        {
            name:'Search',
            path:'/Search/:keyword?',
            meta:{a:true},
            component:()=>import('@/pages/Search/Search.vue')
        },
        {
            name:'Login',
            path:'/Login',
            meta:{a:false},
            component:()=>import('@/pages/Login/Login')
        },
        {
            name:'Register',
            path:'/Register',
            meta:{a:false},
            component:()=>import('@/pages/Register/Register')
        },
        {
            path:'/',
            redirect:"/Home"
        },
        {
            name:'Detail',
            path:"/Detail/:detailid",
            meta:{a:true},
            component:()=>import('@/pages/Detail/Detail')
        },
        {
            name:'addCartSuccess',
            path:'/AddCartSuccess/:count',
            meta:{a:true},
            component:()=> import('@/pages/AddCart/AddCartSuccess.vue')
        },
        {
            name:"shopcar",
            path:'/ShopCar',
            meta:{a:true},
            component:()=>import('@/pages/shopcar/shopcar.vue'),         
        },
        {
            name:"Trade",
            path:"/Trade",
            meta:{a:true},
            beforeEnter:(to,from,next)=>{
                //刷新页面from.path是'/'，而不是'/Trade'
                if(from.path=='/ShopCar'||from.path=='/'||from.path=='/Home'){
                    next()
                }else if(from.path=='/Pay'){
                    next()
                }
                
            },
            component:()=>import('@/pages/Trade/Trade.vue')
        },
        {
            name:'Pay',
            path:'/Pay',
            meta:{a:true},
            beforeEnter (to, from,next) {
                if(from.name=='Trade'||from.path=='/'){
                    next()
                }else{
                    next('/Home')
                }
            },
            component:()=>import('@/pages/pay/Pay.vue')
        },
        {
            name:'PAYSUCCESS',
            path:'/PAYSUCCESS',
            meta:{a:true},
            beforeEnter(){

            },
            component:()=>import('@/pages/PaySuccess/PaySuccess.vue')
        },
        {
            name:'Center',
            path:'/Center',
            meta:{a:true},
            component:()=>import('@/pages/Center/Center.vue'),
            children:[
                {
                    name:'MyTrade',
                    path:'MyTrade',
                    component:()=>import('@/pages/Center/MyTrade/MyTrade.vue')

                },
                {
                    name:'GroupBuyTrade',
                    path:"GroupBuyTrade",
                    component:()=>import('@/pages/Center/GroupBuyTrade/GroupBuyTrade.vue')
                }
            ]
        }
    ],
    //页面的滚动行为
    scrollBehavior(to, from, savedPosition) {
    // 始终滚动到顶部
        return { top: 0 }
    },
})

//定义一个数组，在未登录的情况下，数组里面的路由都不允许跳转，只能去登录页面
let routerarr=['/AddCartSuccess','/ShopCar','/Trade','/Pay','/PAYSUCCESS','/Center/','/Center/MyTrade','/Center/GroupBuyTrade']

//全局路由守卫，监测有没有登录
router.beforeEach((to,from,next)=>{
    let ToPath=to.path
    let token=localStorage.getItem('token')
    //有token就代表已经登录过了
    if(token){
        if(to.path=='/Login'){
            next('/Home')
        }else if(to.path=='/Register'){
            next('/Home')
        }
    //只要没有登录，购物车，订单页，等等之类的页面都不让看，都得先去登录，没有游客购物车这一说！！！！！
    }else{
        //将想去的路由保存下来，在登录之后，点击登录按钮，就跳转到刚刚想去的路由，而不是Home首页
        if(routerarr.indexOf(to.path)!=-1){
            next('/Login?redirect='+ToPath)
        }
    }
    next()
})

export default router