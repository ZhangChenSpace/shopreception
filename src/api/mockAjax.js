//对axios进行二次封装
import axios from 'axios'

//引入进度条
import nProgress from 'nprogress';
//引入进度条的样式
import 'nprogress/nprogress.css'

//1.利用axios对象的方法create,去创建一个axios实例
//2.request就是axios，只不过稍微配置一下
const requests =axios.create({
    //配置对象

    //基础路径，发请求的时候，路径当中会出现
    baseURL:'/mock',
    //代表请求超时的时间(5s)
    timeout:5000,
})

//请求拦截器，在发请求之前，请求拦截器可以监测到，可以在请求发出去之前做一些事情
requests.interceptors.request.use((config)=>{

    //进度条开始
    nProgress.start()

    //config：配置对象，对象里面有一个属性很重视，headers请求头
    return config;
})

//响应拦截器
requests.interceptors.response.use((res)=>{
    //进度条结束
    nProgress.done()
    
    //请求成功了就返回返回体的数据
    return res.data
},(error)=>{
    //请求错误的时候就返回错误信息
    return Promise.reject(error)
})
/* 

*/

//对外暴露
export default requests