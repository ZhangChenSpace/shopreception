//当前这个模块，API进行统一管理
// import { request } from "express";
import requests from "./Ajax";
import mockRequests from './mockAjax'
//三级联动的接口
// /api/product/getBaseCategoryList   get 无参数

export const reqCategoryList = () =>{
    //发请求
    return  requests({url:'/product/getBaseCategoryList',method:'get'})
}

//用mock模拟发送一个get请求，但是并不会真的去向服务器发送一个请求,请求banner数据
export const reqgetBannerList=()=>mockRequests.get('/banner')

export const reqgetfloorList=()=>mockRequests.get('/floor')

//获取搜索数据
//当前这个接口，给服务器传递一个默认参数(至少是一个空对象)
export const reqgetSearchList=(params)=>{
    return requests ({url:'/list',method:'post',data:params})
}

//获取产品详情接口
export const reqItemList=(params)=>{
    return requests ({url :`/item/${params}`,method:'get',})
}

//添加购物车接口
export const reqAddShoppingCart = (skuId, skuNum) => {
    // 返回的结果是promise对象
    return requests({ url: `/cart/addToCart/${skuId}/${skuNum}`, method: "post" })
}

//获取购物车接口
export const reqShopCar=()=>{
    return requests({url:"/cart/cartList",method:'get'})
}

//切换商品选中状态
export const changecheck=(skuID,isChecked)=>{
    return requests({url:`/cart/checkCart/${skuID}/${isChecked}`,method:"get"})
}

//删除选中的商品
export const deletechecked=(skuId)=>{
    return requests({url:`/cart/deleteCart/${skuId}`,method:'DELETE'})
}

//登录接口
export const Login=(params)=>{
    return requests({url:`/user/passport/login`,method:'post',data:params})
}

//获取验证码接口
export const Getidentifyingcode=(phone)=>{
    return requests({url:`/user/passport/sendCode/${phone}`,method:"get"})
}

//注册新用户
export const newuserRegister=( params)=>{
    return requests({url:`/user/passport/register`,method:"post",data:params})
}

//获取用户的信息，带着token
export const getuserinfo=()=>{
    return requests({url:`/user/passport/auth/getUserInfo`,method:'get'})
}

//退出登录
export const logout=()=>{
    return requests({url:`/user/passport/logout`,method:'get'})
}

//获取订单信息
export const Tradeinfo=()=>{
    return requests({url:"/order/auth/trade",method:'get'})
}

//获取地址信息
export const Getaddressinfo=()=>{
    return requests({url:'/user/userAddress/auth/findUserAddressList',method:'get'})
}

//以下的api都不是用vuex去辅助发送的，而且直接引入到组件中，去发送的

//提交订单，跳转到支付界面,不用vuex去实现，在组件内部实现发送请求
export const submitTrade=(tradeNo,data)=>{
    return requests({url:`/order/auth/submitOrder?tradeNo=${tradeNo}`,method:'post',data})
}

//获取订单支付信息
export const reqtradepayinfo=(orderId)=>{
    return requests({url:`/payment/weixin/createNative/${orderId}`,method:'get'})
}

//获取订单的支付状态
export const reqpayinfo=(orderId)=>{
    return requests({url:`/payment/weixin/createNative/${orderId}`,method:'get'})
}

//获取我的订单列表
export const MyTradelist=(page,limit)=>{
    return requests({url:`/order/auth/${page}/${limit}`,method:'get'})
}