import { createApp } from "vue";
import App from './App.vue'
const app = createApp(App)
//使用路由
import router from './router/index'


import '../public/reset.css'
//三级联动组件(全局注册)
import TypeNav from '@/components/TypeNav/typenav.vue'
//分页器，全局组件
import pagination from '@/components/Pagination/Pagination.vue'
//引入MockServer.js-----mock数据
import '@/mock/mockServe'


//全局引入swiper样式，然后在任意一个组件中都能使用swiper轮播图的样式，而不用在各种的组件中再去引入swiper样式
import 'swiper/css/swiper.css'

import myPlugins from "./plugins/myPlugins";

// 引入vuex(我自己用vuex4版本的)
import store from '@/store/index.js'

//注册图片懒加载的插件
// import VueLazyload from 'vue-lazyload'

//图片懒加载，但这个只是插件，而且不好用？，还是得了解原理
// const loadingimg=require('@/assets/aoteman.gif')
// const errorimg=require('@/assets/error.webp')
// app.use(VueLazyload, {
//     preLoad: 1.3,
//     error: errorimg,
//     loading: loadingimg,
//     attempt: 1
// })

//这里是手写的plugin插件，没有功能，只是实验，看看就行~~~~~~
// app.use(myPlugins,{
//     grettings:{
//         hello:'Bonjour'
//     }
// })
app.use(store)
app.component("TypeNav", TypeNav)

//注册分页器这个全局组件
app.component('PaginaTion', pagination)
app.use(router).mount("#app")
