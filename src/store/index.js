import { createStore } from "vuex";
import HomeOptions from '@/store/数据分散/HomeStore.js'
import SearchOptions from '@/store/数据分散/SearchStore.js'
import DetailStore from "./数据分散/DetailStore";
import AddgoodsList from "./数据分散/AddgoodsStore";
import ShopCar from '@/store/数据分散/ShopCarStore'
import logandReg from "./数据分散/LoginandRegisterStore";
import Tradeinfo from './数据分散/TradeStore'

export default createStore({
    modules:{
        //这个是HomeStore被命名了的vuex模块
        HomeOptions,
        SearchOptions,
        DetailStore,
        AddgoodsList,
        ShopCar,
        logandReg,
        Tradeinfo,
    }
})