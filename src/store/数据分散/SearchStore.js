import { reqgetSearchList } from "@/api"
const state={
    SearchList:{}
}
const actions={
    async getSearchlList(context,v){
        let a= await reqgetSearchList(v)
        if(a.code==200){
            context.commit('GETSEARCHLIST',a.data)
        }
    }
}
const mutations={
    GETSEARCHLIST(state,value){
        state.SearchList=value
    } 
}
const getters={
    goodsList(state){
        //当请求SearchList数据失败的时候，就让这些数据返回为空
        return state.SearchList.goodsList||[]
    },
    attrsList(state){
        return state.SearchList.attrsList||[]
    },
    trademarkList(state){
        return state.SearchList.trademarkList||[]
    },
    pageNo(state){
        return state.SearchList.pageNo||[]
    },
    pageSize(state){
        return state.SearchList.pageSize||[]
    },
    total(state){
        return state.SearchList.total||[]
    },
    totalPages(state){
        return state.SearchList.totalPages||[]
    }
}
export default{
    namespaced:true,
    state,
    actions,
    mutations,
    getters
}