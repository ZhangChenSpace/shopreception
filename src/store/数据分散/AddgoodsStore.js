import { reqAddShoppingCart } from '@/api'
const state = {
    Goodslist: {},
}
const actions = {
    async addgoodsStore({ context }, { skuId, skuNum }) {
        let a = await reqAddShoppingCart(skuId, skuNum)
        //代表服务器加入购物车成功
        if (a.code ==200) {
            return '加入购物车成功'
        } else {
            //抛出错误
            return Promise.reject(new Error('error'))
        }
    }

}
const mutations = {

}
const getters = {
}
const modules = {

}
export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
    modules
}