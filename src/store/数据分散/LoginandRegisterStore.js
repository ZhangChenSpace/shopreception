import { Login,Getidentifyingcode,newuserRegister,getuserinfo,logout } from "@/api"
const state={
    code:'',
    token:'',
    userinfo:''
}
const actions={
    //登录接口
    async logining(context,params){
        let a=await Login(params)
        if(a.code==200){
            context.commit('LOGINTOKEN',a.data)
        }
    },
    //获取验证码接口
    async Registering(context,phone){
        let a=await Getidentifyingcode(phone)
        if(a.code==200){
            context.commit('REGISTERING',a.data)
        }
    },
    //新用户注册
    async newUser(context,params){
        let a=await newuserRegister( params)
        //如果成功注册，那么就跳转到登录界面
        if(a.code==200){
            return 'ok'
        }
    },
    //带着token信息，获取用户信息
    async requserinfo(context,value){
        let a=await getuserinfo(value)
        if(a.code==200){
            context.commit('REQUSERINFO',a.data)
        }    
    },
    //退出登录
    async logingout(context){
        let a=await logout()
        if(a.code==200){
            context.commit('LOGINGOUT')
            return 'ok'
        }else{
            return Promise.reject(new Error('错误'))
        }
    }
}
const mutations={
    REGISTERING(state,value){
        state.code=value
    },
    //登录
    LOGINTOKEN(state,value){
        //将token存储在本地存储中
        localStorage.setItem('token',value.token)
        state.token=value.token
    },
    //登录之后获取用户信息
    REQUSERINFO(state,value){
        localStorage.setItem('userinfo',JSON.stringify(value))
        state.userinfo=value
    },
    //退出登录后的事情
    LOGINGOUT(state){
        state.userinfo=''
        localStorage.removeItem('token')
        localStorage.removeItem('userinfo')
    }
}
const getters={

}
export default{
    state,
    actions,
    mutations,
    getters,
    namespaced:true
}