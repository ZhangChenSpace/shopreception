import { Tradeinfo,Getaddressinfo} from "@/api"
const state={
    tradeinfo:'',
    addressinfo:''
}
const actions={
    //获取订单信息
    async reqtradeinfo(context){
        let a=await Tradeinfo()
        if(a.code==200){
            context.commit('REQTRADEINFO',a.data)
        }
    },

    //获取地址信息
    async reqaddressinfo(context){
        let a=await Getaddressinfo()
        if(a.code==200){
            context.commit('REQADDRESSINFO',a.data)
        }
    }
}
const mutations={
    //订单信息
    REQTRADEINFO(state,value){
        state.tradeinfo=value
    },
    //地址信息
    REQADDRESSINFO(state,value){
        state.addressinfo=value
    }
}
const getters={
    orderDetailVoList(state){
        return state.tradeinfo||[]
    },
    totalAmount(state){
        return state.tradeinfo.totalAmount
    },
    totalNum(state){
        return state.tradeinfo.totalNum
    },
    addressinfo(state){
        return state.addressinfo||[]
    },
    tradeno(){
        return state.tradeinfo.tradeNo
    }
}
const modules={

}
export default{
    namespaced:true,
    state,
    actions,
    mutations,
    getters,
    modules
}