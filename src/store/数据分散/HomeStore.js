import { reqCategoryList ,reqgetBannerList,reqgetfloorList} from "@/api"
//Home模块的小仓库
const state={
//state中数据默认初始值别瞎写,要根据服务器返回对象还是数组。
    //三级菜单的数据
    shuju:[],
    //轮播图的数据
    bannerList:[],
    //floor组件的各种数据
    floorList:[]
}
const actions={
    async categoryList(context){
        const a=await reqCategoryList()
        if(a.code==200){
            context.commit('CATEGORYLIST',a.data)
        }
    },
    async getBannerList(context){
        let a= await reqgetBannerList()
        if(a.code==200){
            context.commit('GERBANNERLIST',a.data)
        }
    },
    async getfloorList(context){
        let a=await reqgetfloorList()
        if(a.code==200){
            context.commit('GETFLOORLIST',a.data)
        }
    }
}
const mutations={
    CATEGORYLIST(state,value){
        state.shuju=value
    },
    GERBANNERLIST(state,value){
        state.bannerList=value
    },
    GETFLOORLIST(state,value){
        state.floorList=value
    }
}
const getters={

}
export default{
    namespaced:true,
    state,
    actions,
    mutations,
    getters,
}



// const HomeOptions={
//     namespaced:true,
//     state:{
//         shuju:[]
//     },
//     actions:{
//         async categoryList(context){
//         const a=await reqCategoryList()
//             if(a.code==200){
//                 context.commit('CATEGORYLIST',a.data)
//             }
//         }
//     },
//     mutations:{
//         CATEGORYLIST(state,value){
//             state.shuju=value
//         }
//     },
//     getters:{

//     }
// }
// export default{
//     HomeOptions
// }