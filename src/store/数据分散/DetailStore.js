import { reqItemList } from "@/api";
const state={
    Detaillist:{}
}
const actions={
    async getDetaillist(context ,params){
        let a= await reqItemList(params)
        if(a.code==200){
            context.commit('GETDETAILLIST',a.data)
        }
    }
}
const mutations={
    GETDETAILLIST(state,value){
        state.Detaillist=value
    }
}
const getters={
    categoryView(state){
        return state.Detaillist.categoryView||{}
    },
    skuInfo(state){
        return state.Detaillist.skuInfo||{}
    },
    valuesSkuJson(state){
        return state.Detaillist.valuesSkuJson||{}
    },
    spuSaleAttrList(state){
        return state.Detaillist.spuSaleAttrList||{}
    }
}
const modules={

}
export default{
    namespaced:true,
    state,
    actions,
    mutations,
    getters,
    modules,
}