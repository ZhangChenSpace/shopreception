import { reqShopCar,changecheck, deletechecked } from "@/api"
const state={
    shopcarlist:{},
}
const actions={
    //获取商品列表接口
    async getshopcar(context,value){
        let a= await reqShopCar()
        if(a.code==200){
            context.commit('GETSHOPCAR',a.data)
        }
    },
    //改变商品的选取状态
    async reqchangeckeck({context},{skuID,isChecked}){
        let a=await changecheck(skuID,isChecked)
        // if(a.code==200){
        //     context.commit('CHANGECHECK',a.data)
        // }
    },
    //删除选中的商品
    async deletecheckedshop(context,skuId){
        await deletechecked(skuId)
    }
}
const mutations={
    GETSHOPCAR(state,value){
        state.shopcarlist=value
    }

}
const getters={
    shopcarlist(state){
        return state.shopcarlist[0]||{}
    }
}
export default{
    namespaced:true,
    state,
    actions,
    mutations,
    getters
}