module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',

    //element-ui按需引入的代码
    [
      "@babel/preset-env", { modules: false }
    ]
  ],
  "plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ],

  
}
