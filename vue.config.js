const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  //在项目打包时，不会产生map文件,map文件的作用是项目打包后，js文件是加密的，
  //有了map文件，在项目运行时，可以看到哪里出现了错误，但是为了打包后的体积大小
  //可以删除map文件
  productionSourceMap:false,

  transpileDependencies: true,
  //关闭eslint
  lintOnSave:false,
  //代理跨域
  devServer:{
    proxy:{
      '/api':{
        target:'http://gmall-h5-api.atguigu.cn',
        // pathRewrite:{'^/api':''}
        changeOrigin: true
      }
    }
  },
})

